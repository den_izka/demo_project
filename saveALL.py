import datetime
from pymongo import MongoClient
import json

client = MongoClient('127.0.0.1', 27017)
client.admin.authenticate('root', 'samsung1', mechanism = 'SCRAM-SHA-1')

db = client.mysinoptik
collection = db.weather
collection2 = db.seismicity

s  = {'city': 'Dnipro', 'temp': 95, 'v_wind': 12.9, 'hum': 29, 'time': '2019-03-12 00:44:19.138675'}
b = [{'city': 'NewKiev', 'mag': 1, 'depth': 180, 'time': '2019-03-12 00:49:07.069952'} , {'city': 'OldKiev', 'mag': 8, 'depth': 498, 'time': '2019-03-12 00:49:07.069961'}]

jsonstr = json.dumps(b) 
json_text = json.loads(jsonstr)
#print(isinstance(json_text, list))

if isinstance(json_text, list):
    #json_text = json.loads(jsonstr)
    for diction in json_text:
        #print(i)
        #jsonstr = json.dumps(i)
        #print(jsonstr[0])
        #json_text = json.loads(jsonstr)
        #print(json_text)
        city = diction['city']
        mag = diction['mag']
        depth = diction['depth']
        date = diction['time']
        for time in date:
           time = date[11:19:1]
        new_doc = {'city':city, 'mag':mag, 'depth':depth, 'date':date, 'time_point':time}
        collection2.save(new_doc)
else:
    #jsonstr = json.dumps(s)
    #json_text = json.loads(jsonstr)
    city = json_text['city']
    temp = json_text['temp']
    wind = json_text['v_wind']
    hum = json_text['hum']
    date = json_text['time']
    for time in date:
        time = date[11:19:1]
    new_doc = {'city':city, 'temperature':temp, 'wind_speed':wind, 'humidity':hum, 'date':date, 'time_point':time}
    collection.save(new_doc)