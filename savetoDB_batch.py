import datetime
from pymongo import MongoClient
import json

client = MongoClient('127.0.0.1', 27017)
client.admin.authenticate('root', 'samsung1', mechanism = 'SCRAM-SHA-1')

db = client.mysinoptik

collection2 = db.seismicity

b = [{'city': 'Lviv', 'mag': 1, 'depth': 180, 'time': '2019-03-12 00:49:07.069952'} , {'city': 'Odessa', 'mag': 8, 'depth': 498, 'time': '2019-03-12 00:49:07.069961'}]
jsonstr = json.dumps(b)
json_text = json.loads(jsonstr)
for i in b:
    print(i)
    jsonstr = json.dumps(i)
    print(jsonstr)
    json_text = json.loads(jsonstr)
    print(json_text)
    city = json_text['city']
    mag = json_text['mag']
    depth = json_text['depth']
    date = json_text['time']
    for time in date:
        time = date[11:19:1]
    new_doc = {'city':city, 'mag':mag, 'depth':depth, 'date':date, 'time_point':time}
    collection2.save(new_doc)


