import datetime
from pymongo import MongoClient
import json

def handle(req):
    client = MongoClient('mongodb://main_admin:abc123@mongodb-service.default.svc/mysinoptik', 27017)
    # client.admin.authenticate('main_admin', 'abc123', mechanism = 'SCRAM-SHA-1')

    db = client.mysinoptik
    collection_weth = db.weather
    collection_seis = db.seismicity

    s  = {'city': 'ODESA', 'temp': 95, 'v_wind': 12.9, 'hum': 29, 'time': '2019-03-12 00:44:19.138675'}
    #s = [{'city': 'NewKiev', 'mag': 1, 'depth': 180, 'time': '2019-03-12 00:49:07.069952'} , {'city': 'OldKiev', 'mag': 8, 'depth': 498, 'time': '2019-03-12 00:49:07.069961'}]
    
    jsonstr = json.dumps(s) 
    json_text = json.loads(jsonstr)
    #print(isinstance(json_text, list))

    if isinstance(json_text, list):
        #json_text = json.loads(jsonstr)
        for diction in json_text:
            #print(i)
            #jsonstr = json.dumps(i)
            #print(jsonstr[0])
            #json_text = json.loads(jsonstr)
            #print(json_text)
            city = diction['city']
            mag = diction['mag']
            depth = diction['depth']
            date = diction['time']
            for time in date:
                time = date[11:19:1]
            new_doc = {'city':city, 'mag':mag, 'depth':depth, 'date':date, 'time_point':time}
            collection_seis.save(new_doc)
    else:
        #jsonstr = json.dumps(s)
        #json_text = json.loads(jsonstr)
        city = json_text['city']
        temp = json_text['temp']
        wind = json_text['v_wind']
        hum = json_text['hum']
        date = json_text['time']
        for time in date:
            time = date[11:19:1]
        new_doc = {'city':city, 'temperature':temp, 'wind_speed':wind, 'humidity':hum, 'date':date, 'time_point':time}
        collection_weth.save(new_doc)
            
    #return json.dumps(collection_weth.find_one({"city":"ODESA"}, {"_id": 0}))
