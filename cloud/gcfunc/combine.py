d1= {'Kharkiv': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}, 'Misto Kyyiv': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}, 'Uzhhorod': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}, 'Chernivtsi': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}, 'Dnipropetrovsk': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}, 'Lviv': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}, 'Vinnytsya': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}, 'Kryvyy Rih': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}, 'Odessa': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}, 'Ivano-Frankivsk': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}}
d2= {'Kharkiv': {'main': 'jibk', 'description': 'clear sky', 'time': 1554215400}, 'Misto Kyyiv': {'main': 'Clear', 'description': 'fvfvfv', 'time': 1554215400}, 'Uzhhorod': {'main': 'qqqaaqa', 'description': 'clear sky', 'time': 1554215400}, 'Chernivtsi': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}, 'Dnipropetrovsk': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}, 'Lviv': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}, 'Vinnytsya': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}, 'Kryvyy Rih': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}, 'Odessa': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}, 'Ivano-Frankivsk': {'main': 'Clear', 'description': 'clear sky', 'time': 1554215400}}

def findDiff(d1, d2, path=""):
    for k in d1.keys():
        if not d2.has_key(k):
            print (path)
            print (k + " as key not in d2", "\n")
        else:
            if type(d1[k]) is dict:
                if path == "":
                    path = k
                else:
                    path = k
                findDiff(d1[k],d2[k], path)
            else:
                if d1[k] != d2[k]:
                    print (path)
                    print ("changed " + k + " : " + d2[k]) 

print ("comparing d1 to d2:")
print (findDiff(d1,d2))