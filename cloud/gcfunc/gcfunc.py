import base64
import json
import redis

CURR_VALUE = redis.StrictRedis(host='35.187.98.192', port=6379, db=0)

curr_tmp_values_dic = {}
current_forecast_dic = {}

gen_tmp_values_dic = {}
generated_full_dic = {}


for k in CURR_VALUE.keys('*'):
    tmp = CURR_VALUE.get(k)
    d = tmp.decode('utf-8')
    a = json.loads(d)
    city = a['name']
    curr_weath = a['weather']
    cwd = curr_weath[0]
    main = cwd['main']
    description = cwd['description']
    time = a['dt']
    gen_tmp_values_dic.update([('main', main) , ('description', description) , ('time' , time)])
    current_forecast_dic[city] = gen_tmp_values_dic
    
print(current_forecast_dic)

GEN_VALUE = redis.StrictRedis(host='35.187.98.192', port=6379, db=0)

for k in GEN_VALUE.keys('*'):
    tmp = GEN_VALUE.get(k)
    d = tmp.decode('utf-8')
    a = json.loads(d)
    city = a['name']
    curr_weath = a['weather']
    cwd = curr_weath[0]
    main = cwd['main']
    description = cwd['description']
    time = a['dt']
    gen_tmp_values_dic.update([('main', main) , ('description', description) , ('time' , time)])  
    generated_full_dic[city] = gen_tmp_values_dic

print(generated_full_dic)

def findDiff(current_forecast_dic, generated_full_dic, path=""):
    for k in current_forecast_dic.keys():
        if not generated_full_dic.has_key(k):
            print (path)
            print (k + " as key not in generated_full_dic", "\n")
        else:
            if type(current_forecast_dic[k]) is dict:
                if path == "":
                    path = k
                else:
                    path = k
                findDiff(current_forecast_dic[k],generated_full_dic[k], path)
            else:
                if current_forecast_dic[k] != generated_full_dic[k]:
                    print (path)
                    print ("changed " + k + " : " + generated_full_dic[k]) 

print ("comparing current_forecast_dic to generated_full_dic:")
print (findDiff(current_forecast_dic,generated_full_dic))