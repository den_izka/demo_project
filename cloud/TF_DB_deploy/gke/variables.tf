#################
### Variables ###
#################
variable "project" {}

variable "region" {
  default = "europe-west1-b"
}

variable "username" {
  default = "admin"
}

variable "password" {}
