forecast_text = {
    "Settled fine": ":sunny:",
    "Fine weather": ":sunny:",
    "Becoming fine": ":sunny:",
    "Fine, becoming less settled": ":sunny:",
    "Fine, possible showers": 	":umbrella:",
    "Fairly fine, improving": ":sunny:",
    "Fairly fine, possible showers early": ":umbrella:",
    "Fairly fine, showery later": ":umbrella:",
    "Showery early, improving": 	":umbrella:",
    "Changeable, mending": ":partly_sunny:",
    "Fairly fine, showers likely": 	":umbrella:",
    "Rather unsettled clearing later": ":partly_sunny:",
    "Unsettled, probably improving": ":partly_sunny:",
    "Showery, bright intervals": ":umbrella:",
    "Showery, becoming less settled": ":umbrella:",
    "Changeable, some rain": ":umbrella:",
    "Unsettled, short fine intervals": ":partly_sunny:",
    "Unsettled, rain later": ":partly_sunny:",
    "Unsettled, some rain": ":umbrella:",
    "Mostly very unsettled": ":partly_sunny:",
    "Occasional rain, worsening": ":umbrella:",
    "Rain at times, very unsettled": ":umbrella:",
    "Rain at frequent intervals": ":umbrella:",
    "Rain, very unsettled": ":umbrella:",
    "Stormy, may improve": ":zap:",
    "Stormy, much rain": ":zap:"
}

word = message.text
if word in forecast_text:
    elem = forecast_text[word]
    emoji = elem