import datetime
from pymongo import MongoClient
import json

def handle(req):
    """handle a request to the function
    Args:
        req (str): request body
    """
    client = MongoClient('mongodb://main_admin:abc123@mongodb-service.default.svc/mysinoptik', 27017)

    db = client.mysinoptik
    collection_weth = db.weather
    collection_seis = db.seismicity
    json_text = json.loads(req)

    if isinstance(json_text, list):
        for diction in json_text:
            city = diction['city']
            mag = diction['mag']
            depth = diction['depth']
            date = diction['time']
            for time in date:
                time = date[11:19:1]
            new_doc = {'city':city, 'mag':mag, 'depth':depth, 'date':date, 'time_point':time}
            collection_seis.save(new_doc)
    else:
        city = json_text['city']
        temp = json_text['temp']
        wind = json_text['v_wind']
        hum = json_text['hum']
        date = json_text['time']
        for time in date:
            time = date[11:19:1]
        new_doc = {'city':city, 'temperature':temp, 'wind_speed':wind, 'humidity':hum, 'date':date, 'time_point':time}
        collection_weth.save(new_doc)